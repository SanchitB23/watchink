# WatchInk

- [WatchInk](#watchink)
    - [Useful Links](#useful-links)
    - [Bugs List](#bugs-list)
        - [Version 1.0](#version-10)
            - [Design](#design)
            - [Code Related](#code-related)
    - [Change Log](#change-log)
        - [v1.0](#v10)
    - [TODO](#todo)

## Useful Links

- [Feedback Form and Responses](https://docs.google.com/forms/d/11388R8UkeHg_df65Hr-X8BN67IMj3hay28ZmE0KemN0/edit#responses)
- [Bitbucket Repository Link](https://bitbucket.org/SanchitB23/watchink)
- [Trello Link](https://trello.com/b/tos3GyKI/watchink)
- [Figma Link](https://www.figma.com/file/LmF3kD2lh9rbsFv4zFb7Qr5b/WatchInk?node-id=0%3A1)

---

## Bugs List

### Version 1.0

> [Version 1.0 APK Link](https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40sanchitb23/watchink-5438a78723dee5bdb2c580bf2b71dfb9-signed.apk)

#### Design

1. Splash Screen:
    - Image being centered not full size
        - [x] Solution:  change to cover instead of contain.
2. Movies Home
    1. Background photo is small from bottom
        - [ ] Solution:
3. All Screens:
    - Components getting cropped
        - [ ] Solution: Use `%` wherever possible instead of hard coded CSS
        - Reference:
            - [Tips To Develop React Native for All Screen Sizes](https://medium.com/@shanerudolfworktive/7-tips-to-develop-react-native-uis-for-all-screen-sizes-7ec5271be25c)

#### Code Related

1. Need Solution for VPN:
    1. App restarts when no VPN found
    - [ ] Solution:

---

## Change Log

### v1.0

1. Added Welcome Screen
2. Added Login with Email
3. Added Register With Email
4. Added Forget Password
5. Added Verify Email on Register
6. Added SideNav Bar
   1. Account Details Icon Color to `Red` if not Registered
   2. Added Log Out Button
   3. Added Feedback form link on Sidebar Nav
7. Added Movies in Tab
   4. Added Search Function
   5. Added onMovieClick
8. Added Movie Details Page
   6. Redirects to YouTube Trailer
   7. Can Download Torrents

---

## TODO

<!--TODO List>
Todo List follows:
<!-->
1. Movie Details:
   - [ ] Do something about Cast Component
   - [ ] Text on Tile to adjust with background Photo
   - [ ] onBackClick take to MoviesHome
   - [ ] Add Suggestions
   - [ ] Header:
      - [ ] Add Back Arrow on top
      - [ ] Add Favorite Button
2. General:
   - [x] Make UI Screen Size Friendly
3. Auth:
   - [ ] Add FaceBook Auth
   - [ ] Add Google Auth