import React, { Component } from 'react';
import {
  createStackNavigator,
  createDrawerNavigator,
  createBottomTabNavigator
} from 'react-navigation';
import { Icon } from 'react-native-elements';

import WelcomeScreen from './components/screens/WelcomeScreen.js';
import LoginScreen from './components/screens/LoginScreen';
import RegisterScreen from './components/screens/RegisterScreen';
import MoviesHome from './components/screens/MoviesHome';
import onMovieClick from './components/screens/onMovieClickScreen';

import TVShowsHome from './components/screens/TVShowsHome';
import BooksHome from './components/screens/BooksHome';
import SideMenu from './components/SideMenu';

export default class NavigationRoot extends Component {
  componentDidMount() {
    console.log('NavigationRoot');
  }
  render() {
    const authStack = createStackNavigator(
      {
        login: { screen: LoginScreen },
        register: { screen: RegisterScreen }
      },
      {
        headerMode: 'none',
      }
    );
    const MoviesStack = createStackNavigator(
      {
        home: { screen: MoviesHome },
        on_click: { screen: onMovieClick }
      },
      {
        headerMode: 'none',
      }
    );
    const homeTabs = createBottomTabNavigator(
      {
        movies_home: {
          screen: MoviesStack,
          navigationOptions: {
            title: 'Movies',
            tabBarIcon: ({ tintColor }) => <Icon name='movie' color={tintColor} />
          }
        },
        tvShows_home: {
          screen: TVShowsHome,
          navigationOptions: {
            title: 'TV Shows',
            tabBarIcon: ({ tintColor }) => <Icon name='tv' type='feather' color={tintColor} />
          }
        },
        books_home: {
          screen: BooksHome,
          navigationOptions: {
            title: 'Books',
            tabBarIcon: ({ tintColor }) => <Icon
              name='book-open-page-variant'
              type='material-community'
              color={tintColor}
            />
          }
        }
      }, {
        tabBarOptions: {
          labelStyle: {
            fontSize: 14,
            textAlign: 'center',
            alignSelf: 'center',
          },
          showIcon: true,
          style: {
            backgroundColor: 'rgba(29, 36, 42, 0.96)'
          }
        }
      }
    );
    const sideNavDrawer = createDrawerNavigator(
      {
        home: { screen: homeTabs },
        welcome: {
          screen: WelcomeScreen,
          navigationOptions: () => ({
            drawerLockMode: 'locked-closed'
          })
        },
        auth: {
          screen: authStack,
          navigationOptions: () => ({
            drawerLockMode: 'locked-closed'
          })

        }
      },
      {
        contentComponent: props => <SideMenu {...props} />
      }
    );

    const MainNavigator = createStackNavigator(
      {
        welcome: { screen: WelcomeScreen },
        auth: { screen: authStack },
        main: { screen: sideNavDrawer }
      },
      {
        headerMode: 'none',
        // initialRouteName: 'main' // TEMP:
      }
    );
    return (
      <MainNavigator />
    );
  }
}
