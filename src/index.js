import React, { Component } from 'react';
import firebase from 'firebase';
import { Provider } from 'react-redux';

import store from './utils/store';
import NavigationRoot from './NavigationRoot';

export default class MainApp extends Component {

  componentWillMount() {
    this.initializeFirebase();
  }
  componentDidMount() {
    console.info('src:index');
  }

  initializeFirebase() {
    const config = {
      apiKey: 'AIzaSyBX6VVsCKQ1OuT7v3Thq7WR1__pa9s6V5A',
      authDomain: 'watchinkmobile.firebaseapp.com',
      databaseURL: 'https://watchinkmobile.firebaseio.com',
      projectId: 'watchinkmobile',
      storageBucket: 'watchinkmobile.appspot.com',
      messagingSenderId: '9420543616'
    };
    firebase.initializeApp(config);
  }
  render() {
    return (
      <Provider store={store}>
        <NavigationRoot />
      </Provider>
    );
  }
}


// NOTE: Put All Notes in this File
// TODO: Handle Hardware back press hint: LoginScreens
// TODO: add onChange for inputs in login and register
// TODO: add Firebase auth
// REVIEW: Login and Sign up complete after ToDo 17,18,19
// TODO: // renderButton for loading
// XXX: got token check actions
// QUESTION: how to check and login with token
