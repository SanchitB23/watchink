import * as Strings from '../utils/strings';

const INITIAL_STATE = {
  loading: false,
  error: '',
  user: null
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Strings.LOGIN_EMAIL_USER:
      return { ...state, loading: true, error: '' };

    case Strings.LOGIN_EMAIL_FAIL:
      return { ...state, error: action.payload, loading: false };

    case Strings.LOGIN_EMAIL_SUCCESS:
      return { ...state, ...INITIAL_STATE, user: action.payload };
    case Strings.REGISTER_EMAIL_USER:
      return { ...state, loading: true, error: '' };

    case Strings.REGISTER_EMAIL_FAIL:
      return { ...state, error: action.payload, loading: false };

    case Strings.REGISTER_EMAIL_SUCCESS:
      return { ...state, ...INITIAL_STATE };
    default:
      return state;
  }
};
