import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import { Tile } from 'react-native-elements';

import data from './moviedetail.json';

class TestComp extends React.Component {
  render() {
    console.log(data.data.movie);
    return (
      <ImageBackground
        resizeMode='cover'
        source={{ uri: data.data.movie.large_cover_image }}
        style={{ width: '100%', height: '100%' }}
        blurRadius={1}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.6)', flex: 1 }}>
          <Tile
            imageSrc={{ uri: data.data.movie.large_cover_image }}
            icon={{
              name: 'youtube-play',
              type: 'font-awesome',
              color: '#fff',
              size: 46,
              onPress: () => console.log('ldsf')
            }}
          // activeOpacity={} //// BUG:
          // title="Lorem ipsum dolor sit amet, consectetur"
          >
            <View style={{ marginTop: -90 }}>
              <Text style={[styles.textStyle, styles.titleStyle]}>{data.data.movie.title}</Text>
              <View
                style={{ flex: 1, flexDirection: 'row' }}
              >
                <Text style={styles.textStyle}>caption</Text>
                <Text style={styles.textStyle}>caption</Text>
                <Text style={styles.textStyle}>caption</Text>
              </View>

            </View>
          </Tile>
        </View>
      </ImageBackground>
    );
  }
}
//·
export default TestComp;
const styles = {
  textStyle: {
    color: '#fff',
    fontSize: 16
  },
  titleStyle: {
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 5
  }
};
