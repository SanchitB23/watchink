import firebase from 'firebase';
import { AsyncStorage } from 'react-native';

import * as Strings from '../utils/strings.js';

export const loginWithEmail = ({ email, password }, navigation) => async (dispatch) => {
  dispatch({ type: Strings.LOGIN_EMAIL_USER });
  try {
    const firebaseRequest = await firebase.auth().signInWithEmailAndPassword(email, password);

    const value = await AsyncStorage.getItem('first_launch');
    if (value === null) {
      await AsyncStorage.setItem('first_launch', new Date());
    } else {
      console.log(navigation);
      loginUserWithEmailSuccess(dispatch, firebaseRequest, navigation);
    }
  } catch (e) {
    let error;
    switch (e.code) {
      case 'auth/invalid-email':
        error = 'Invalid Email';
        break;
      case 'auth/user-not-found':
        error = 'User Not Found';
        break;
      case 'auth/wrong-password':
        error = 'Wrong Password';
        break;
      default:
        error = e.message;
    }
    loginUserWithEmailFail(dispatch, error);
  } finally {
    // navigation.navigate('main'); // HACK: navigation working in actions
  }
};

const loginUserWithEmailSuccess = (dispatch, user, navigation) => {
  dispatch({
    type: Strings.REGISTER_EMAIL_SUCCESS,
    payload: user
  });
  console.log('Login Success');
  navigation.navigate('main');
};
const loginUserWithEmailFail = (dispatch, error) => {
  dispatch({
    type: Strings.LOGIN_EMAIL_FAIL,
    payload: error
  });
  console.log('Login Fail');
};

export const registerWithEmail = (values, navigation) => async (dispatch) => {
  console.log(values);
  dispatch({ type: Strings.REGISTER_EMAIL_USER });
  try {
    await firebase.auth().createUserWithEmailAndPassword(
      values.email, values.password
    );
    const user = await firebase.auth().currentUser;
    await user.updateProfile({
      displayName: values.name
    });
    console.log('registerUser', user);
    try {
      const t = await user.sendEmailVerification();
      console.log(t);
    } catch (e) {
      console.error(e);
    } finally {
      registerUserWithEmailSuccess(dispatch, navigation);
    }
  } catch (e) {
    console.log('registerError', e);
    let error;
    switch (e.code) {
      case 'auth/email-already-in-use':
        console.log(e.message);
        error = e.message;
        break;
      case 'auth/invalid-email':
        console.log(e.message);
        error = e.message;
        break;
      case 'auth/operation-not-allowed':
        console.log(e.message);
        error = e.message;
        break;
      case 'auth/weak-password':
        console.log(e.message);
        error = e.message;
        break;
      default:
        console.log(e.message);
        error = e.message;
    }
    registerUserWithEmailFail(dispatch, error);
  }
};

const registerUserWithEmailSuccess = (dispatch, navigation) => {
  dispatch({
    type: Strings.REGISTER_EMAIL_SUCCESS,
  });
  console.log('Register Success');
  navigation.navigate('login');
};
const registerUserWithEmailFail = (dispatch, error) => {
  dispatch({
    type: Strings.REGISTER_EMAIL_FAIL,
    payload: error
  });
  console.log('Register Fail');
};

// TODO: screen loader
// TODO: reducer
// TODO: action caller
