import { Dimensions } from 'react-native';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

export const SLIDES_DATA = [
  {
    bg: require('../../assets/images/welcome_scr/bg_movies_scr.png'),
    icon: require('../../assets/images/welcome_scr/ic_movie.png'),
    text: 'Movies',
    height: 66,
    width: 70
  },
  {
    bg: require('../../assets/images/welcome_scr/bg_tv_scr.png'),
    icon: require('../../assets/images/welcome_scr/ic_tv.png'),
    text: 'Tv Shows',
    height: 65,
    width: 65
  },
  {
    bg: require('../../assets/images/welcome_scr/bg_books_scr.png'),
    icon: require('../../assets/images/welcome_scr/ic_book.png'),
    text: 'Books',
    height: 65,
    width: 83
  }
];

export const YTS_MOVIES_LIST_API = 'https://yts.am/api/v2/list_movies.json';
export const YTS_MOVIE_DETAILS_API = 'https://yts.am/api/v2/movie_details.json?with_cast=true&movie_id=';
