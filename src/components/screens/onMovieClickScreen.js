//Bg image
//Tile on top flex 1
// view flex 2
//Left: Flatlist with header footer (persistant)
//Center/Right:
//View flex 2 opacity light
//Text large title
//Text summary
//View opacity 0
//button 720
//button 1080

import React, { Component } from 'react';
import {
  View, Text, ImageBackground, ActivityIndicator, Linking, FlatList, TouchableOpacity, ScrollView
} from 'react-native';
import { Tile, Icon, Card, Button } from 'react-native-elements';
import axios from 'axios';
import { Font } from 'expo';
import moment from 'moment';

import * as ConstantsStrings from '../../utils/constants';

class onMovieClickScreen extends Component {
  state = {
    result: null,
    isLoading: false,
    fetchError: false,
    fontLoaded: false
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto_medium: require('../../../assets/fonts/Roboto-Light.ttf')
    });

    this.setState({ fontLoaded: true });
  }
  componentDidMount() {
    const itemId = this.props.navigation.getParam('itemId', 10);
    console.log('Movie ID', itemId);
    this.loadYTSDetailAPI(itemId);
  }

  loadYTSDetailAPI = async (id) => {
    this.setState({ isLoading: true });
    const url = ConstantsStrings.YTS_MOVIE_DETAILS_API + id;
    try {
      const request = await axios.get(url);
      const result = request.data.data.movie;
      this.setState({ result });
    } catch (e) {
      console.error(e);
      this.setState({ fetchError: true });
    } finally {
      this.setState({ isLoading: false });
    }
  }


  renderCastList = (item) => (
    <View style={{ flex: 1 }}>
      <TouchableOpacity onPress={() => Linking.openURL(`https://www.imdb.com/name/nm${item.item.imdb_code}/`)}>
        <Card
          containerStyle={styles.CardContainerStyle}
        >
          <Text style={styles.castTextStyle}>{item.item.character_name}</Text>
          <Text style={styles.castTextStyle}>As</Text>
          <Text style={styles.castTextStyle}>{item.item.name}</Text>
        </Card>
      </TouchableOpacity>
    </View>
  )
  // REVIEW: Tile Done: Text Color to something universal/ back blurry background(try)
  render() {
    const { result, fontLoaded } = this.state;
    if (!result || !fontLoaded) {
      return (
        <View style={styles.loaderViewStyle}>
          <ActivityIndicator size='large' color='#B71C1C' />
        </View>
      );
    }
    const runtime = parseFloat(moment.duration(result.runtime, 'minutes').asHours()).toFixed(2);
    return (
      // Full View
      <ImageBackground
        resizeMode='cover'
        source={{ uri: result.large_cover_image }}
        style={{ width: '100%', height: '100%' }}
        blurRadius={1}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.6)', flex: 1 }}>
          {/* Tile Top*/}
          <Tile
            imageSrc={{ uri: result.large_cover_image }}
            icon={{
              name: 'youtube-play',
              type: 'font-awesome',
              color: '#fff',
              size: 46,
              onPress: () => Linking.openURL(`https://www.youtube.com/watch?v=${result.yt_trailer_code}`)
            }}

            containerStyle={{ height: ConstantsStrings.SCREEN_HEIGHT / 3, marginBottom: -20 }}
          // activeOpacity={} //// BUG:
          >
            <View style={{ marginTop: -90, marginBottom: 20 }}>
              <Text
                style={[styles.textStyle, styles.titleStyle, {
                  fontFamily: 'Roboto_medium'
                }]}
              >{result.title}</Text>
              <View
                style={{ flex: 1, flexDirection: 'row' }}
              >

                <Text style={styles.textStyle}>{result.year}</Text>
                <Text style={styles.textStyle}> • </Text>
                <Text style={styles.textStyle}>
                  {
                    result.genres.length > 1 ? (
                      `${result.genres[0]}/${result.genres[1]}`
                    ) : result.genres[0]
                  }
                </Text>
                <Text style={styles.textStyle}> • </Text>
                <Text style={styles.textStyle}>{runtime} hours</Text>
              </View>
            </View>
          </Tile>
          {/*After Top Tile View*/}
          <View style={{ flex: 1, flexDirection: 'row' }}>
            {
              result.cast ? (
                <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', flex: 1 }} >
                  {/* Cast Header */}
                  <View style={styles.castHeaderStyle}>
                    <Icon
                      name='imdb'
                      type='font-awesome'
                      color="#f5de50"
                      size={35}
                      iconStyle={{ width: 45 }}
                      onPress={() => Linking.openURL(`https://www.imdb.com/title/${result.imdb_code}`)}
                    />
                    <Text style={styles.textStyle}>
                      {result.rating}
                    </Text>
                  </View>
                  {/* Cast List */}
                  <View style={{ flex: 0.8 }}>
                    <FlatList
                      data={result.cast}
                      renderItem={this.renderCastList}
                      ListHeaderComponent={
                        () => <Text style={[styles.textStyle, { textAlign: 'center' }]}>Cast</Text>
                      }
                      keyExtractor={(item, index) => index.toString()}
                    />
                  </View>
                  <View style={{ flex: 0.1, flexDirection: 'row' }}>
                    <Text
                      style={{ color: '#fff' }}
                      onPress={() => Linking.openURL(`https://www.imdb.com/title/${result.imdb_code}`)}
                    >
                      For more, goto IMDB.com >
                    </Text>
                  </View>
                </View>
              ) : <View />
            }
            <View style={{ flex: 3 }} >
              <View style={styles.descriptionViewStyle} >
                <Text style={[styles.textStyle, { fontSize: 20 }]}>Overview: </Text>
                <ScrollView>
                  <Text style={[styles.textStyle, { fontSize: 14, letterSpacing: 1 }]}>
                    {result.description_full}
                  </Text>
                </ScrollView>
              </View>
              <View style={{ flex: 1, justifyContent: 'space-evenly' }} >
                {
                  result.torrents.map((url, index) => (
                    <Button
                      key={index}
                      title={`Download ${url.quality}${index > 1 ? ' BluRay' : ''}`}
                      textStyle={{ fontWeight: 'bold' }}
                      rounded
                      backgroundColor='#162B37'
                      onPress={() => Linking.openURL(`magnet:?xt=urn:btih:${url.hash}&dn=${result.slug}-${url.quality}&tr=http://track.one:1234/announce&tr=udp://track.two:80`)}
                    />
                  )
                  )
                }
              </View>
            </View>
          </View>
        </View>
      </ImageBackground >
    );
  }
}

export default onMovieClickScreen;

const styles = {
  textStyle: {
    color: '#fff',
    fontSize: 16
  },
  titleStyle: {
    fontSize: 28,
    // fontWeight: 'bold',
    marginBottom: 5,
    // backgroundColor: '#000',
    // opacity: 0.5
  },
  loaderViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  castHeaderStyle: {
    flex: 0.1,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 10,
  },
  CardContainerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.10)',
    borderWidth: 0,
    flex: 1
  },
  descriptionViewStyle: {
    backgroundColor: 'rgba(0,0,0,0.45)',
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  castTextStyle: {
    textAlign: 'center',
    color: '#fff',
  }
};
