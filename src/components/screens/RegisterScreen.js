import { View, BackHandler, ImageBackground, Text, TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, reset } from 'redux-form';
import { Font, Constants } from 'expo';
import {
  Header,
  Left,
  Body,
  Title,
  Footer,
  Form,
  Input,
  Label,
  Item,
} from 'native-base';
import { Button, Icon, SocialIcon } from 'react-native-elements';

import { registerWithEmail } from '../../actions';

class RegisterScreen extends Component {

  constructor() {
    super();
    this.state = { fontLoaded: false };
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto_medium: require('../../../assets/fonts/Roboto-Light.ttf')
    });

    this.setState({ fontLoaded: true });
    console.log('register');
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack(null);
    return true;
  }

  onSubmit(values) {
    this.props.registerWithEmail(values, this.props.navigation);
  }

  //render Methods
  renderField(field) {
    return (
      <View>
        <Item stackedLabel>
          <Label style={styles.textColor}>{field.label}</Label>
          <Input
            style={styles.textColor}
            secureTextEntry={field.secureTextEntry}
            {...field.input}
            keyboardType={field.keyboardType}
          />
        </Item>
        <Text style={{ color: '#D50000', marginTop: '1%' }}>
          {field.meta.touched ? field.meta.error : ''}
        </Text>
      </View>
    );
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <ImageBackground
        source={require('../../../assets/images/bg_register.png')}
        resizeMode='contain'
        style={{ height: '100%', width: '100%', flex: 1 }}
      >
        <View style={{ height: Constants.statusBarHeight, backgroundColor: '#373E43' }} />
        <Header span style={{ backgroundColor: 'transparent' }}>
          <Left>
            <Icon
              name='arrow-left'
              type='feather'
              color="#fff"
              style={{ marginLeft: 35 }}
              onPress={() => this.props.navigation.navigate('welcome')}
            />
          </Left>
          <Body >
            {
              this.state.fontLoaded ? (
                <Title style={{ fontSize: 40, textAlign: 'left' }}>Sign Up</Title>
              ) : null
            }
          </Body>
        </Header>
        <View style={{ flex: 1 }}>
          <Form style={styles.formStyle}>
            <Field
              label='Name'
              name='name'
              component={this.renderField}
              secureTextEntry={false}
              keyboardType='default'
            />
            <Field
              label='Email Address'
              name='email'
              component={this.renderField}
              secureTextEntry={false}
              keyboardType='email-address'
            />
            <Field
              label='Password'
              name='password'
              component={this.renderField}
              secureTextEntry
              keyboardType='default'
            />
          </Form>

          <View style={styles.firebaseErrorStyle}>
            <Text style={{ color: '#BF1616' }}>{this.props.error}</Text>
          </View>
          <View style={styles.buttonView}>
            <Button
              title='Sign Up'
              buttonStyle={styles.registerButton}
              onPress={this.props.loading ? null : handleSubmit(this.onSubmit.bind(this))}
              loading={this.props.loading}
            />
            <Text
              style={{ marginVertical: '2.5%', color: '#9EA1A3' }}
            >Or, Sign Up with</Text>
            <View style={{ flexDirection: 'row' }}>
              <SocialIcon
                type='facebook'
                button
                title='Facebook'
                style={{ paddingHorizontal: '3%' }}
              />
              <SocialIcon
                type='google-plus-official'
                button
                title='Google'
                style={{ paddingHorizontal: '3%' }}
              /></View>
          </View>
        </View>
        <Footer style={{ backgroundColor: '#C4C4C423', height: '4.57%', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('login')}>
            <Text style={{ color: '#9EA1A3' }}>
              Already have an account?
                <Text style={{ color: '#BF1616' }}>
                Sign In
                </Text>
            </Text>
          </TouchableOpacity>
        </Footer>
      </ImageBackground>
    );
  }
}

function validate(values) {
  const errors = {};
  if (!values.name) {
    errors.name = 'Enter Name';
  }
  if (!values.email) {
    errors.email = 'Enter Email Address';
  }
  if (!values.password) {
    errors.password = 'Enter Password';
  }
  return errors;
}
const mapStateToProps = ({ auth }) => {
  const { error, loading } = auth;
  return { error, loading };
};

const onSubmitSuccess = (result, dispatch) => {
  dispatch(reset('RegisterForm'));
};

const styles = {
  registerButton: {
    backgroundColor: '#BF1616',
    borderRadius: 8,
    paddingHorizontal: '4%'
  },
  textColor: {
    color: '#9FA0A2',
  },
  buttonView: {
    marginTop: '4%',
    alignItems: 'center'
  },
  firebaseErrorStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  formStyle: {
    paddingHorizontal: '5%',
    justifyContent: 'center'
  }
};

export default reduxForm({
  validate,
  form: 'RegisterForm',
  onSubmitSuccess
})(connect(mapStateToProps, { registerWithEmail })(RegisterScreen));
