import React from 'react';
import {
  View,
  Text,
  FlatList,
  ImageBackground,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl
} from 'react-native';
import { Constants } from 'expo';
import { Card, SearchBar } from 'react-native-elements';
import {
  Left,
  Right,
  Body
} from 'native-base';
import axios from 'axios';

import * as ConstantsStrings from '../../utils/constants';

// BUG: Handle Memory leak
/*
Warning: Can't call setState (or forceUpdate) on an unmounted component.
This is a no-op, but it indicates a memory leak in your application.
To fix, cancel all subscriptions and asynchronous tasks in the componentWillUnmount method.
*/
// TODO: Status bar
class MoviesHome extends React.PureComponent {
  static navigationOptions = () => (
    {
      tabBarLabel: 'Movies'
    }
  )
  //component Level States
  state = {
    page: 1,
    // data: [],
    isLoading: false,
    isRefreshing: false,
    searched: false,
    searchTerm: '',
    fetchError: false
  };

  componentWillMount() {
    console.log('Movies Home');
    this.loadYTSAPI();
  }
  //Clear Button Pressed on Search Bar
  onSearchClear = async () => {
    await this.setState({ searched: false, apiData: null, page: 1 });
    this.loadYTSAPI();
  }
  onRefresh = async () => {
    this.setState({ isRefreshing: true });
    await this.loadYTSAPI();
    this.setState({ isRefreshing: false });
  }
  //API call function
  loadYTSAPI = async () => {
    const { apiData, searched, page, searchTerm } = this.state;
    this.setState({ isLoading: true });
    try {
      let url = '';
      if (searched) {
        url = `${ConstantsStrings.YTS_MOVIES_LIST_API}?page=${page}&query_term='${searchTerm}'`;
      } else {
        url = `${ConstantsStrings.YTS_MOVIES_LIST_API}?page=${page}&sort_by=year`;
      }
      const res = await axios.get(url);
      const result = res.data.data.movies;
      const temp = res.data.data;
      await this.setState({ apiData: {} });
      this.setState({
        apiData: page === 1 ? temp.movie_count === 0 ? []
          : result
          : (temp.movie_count / temp.limit) > temp.page_number
            ? [...apiData, ...result]
            : [...apiData],
        isRefreshing: false,
        isLoading: false
      });
    } catch (e) {
      // console.error('Movies API YTS: ', e);
      this.setState({ fetchError: true });
    }
  }

  //on Reach End function CallBack
  handleMoreListData = () => {
    this.setState({
      page: this.state.page + 1
    }, () => {
      this.loadYTSAPI();
    });
  }

  //on Movie Searched
  searchMovie = async () => {
    await this.setState({ searched: true, apiData: null, page: 1 });
    this.loadYTSAPI();
  }


  // Components

  //renderList
  renderMovieListItem = (data) => {
    const { item } = data;
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('on_click', {
            itemId: item.id
          })}
        >
          <Card
            image={{ uri: item.large_cover_image }}
            imageProps={{ resizeMode: 'cover' }}
            imageStyle={{ height: 250 }}
            containerStyle={styles.CardContainerStyle}

          >
            <View style={{ flex: 1 }}>
              <Text style={styles.cardTextStyle}>{item.title}</Text>
              <Text style={styles.cardTextStyle}>{item.year}</Text>
            </View>
          </Card>
        </TouchableOpacity>
      </View>
    );
  }

  //empty list render
  renderEmptyList = () => (
    // TODO: Make it look better
    <Card containerStyle={styles.CardContainerStyle}>
      <Text style={[styles.cardTextStyle, { textAlign: 'center' }]}>No Movies</Text>
    </Card>
  )

  //loading component footer
  renderFooterForFlatList = () => (
    <View style={{ justifyContent: 'center' }}>
      {this.state.isLoading ? <ActivityIndicator color='#EF5350' /> : null}
    </View>
  )


  //main
  render() {
    console.log(ConstantsStrings.SCREEN_HEIGHT, ConstantsStrings.SCREEN_WIDTH);
    return (

      <ImageBackground
        source={require('../../../assets/images/movies_home_scr/bg_movies_home.png')}
        resizeMode='cover'
        style={styles.imageBackgroundStyle}
      >
        <View style={{ height: Constants.statusBarHeight }} />

        {/* Header Component */}
        <View style={styles.headerStyle}>
          <Left style={{ marginLeft: 20 }}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.openDrawer()}>
              <Image
                source={require('../../../assets/images/movies_home_scr/ic_nav.png')}
                style={{ height: 21, width: 34 }}
              />
            </TouchableWithoutFeedback>
          </Left>
          <Body style={styles.headerBodyStyle}>
            <Text style={styles.tabHeaderStyle}>Movies</Text>
          </Body>
          <Right>
            <SearchBar
              clearIcon
              round
              // showLoadingIcon
              onChangeText={(searchTerm) => this.setState({ searchTerm })}
              placeholder='Search Movies...'
              containerStyle={styles.searchBarContainerStyle}
              returnKeyType='search'
              onClearText={this.onSearchClear}
              onSubmitEditing={this.searchMovie}
            />
          </Right>
        </View>

        {
          this.state.apiData ? (
            <FlatList
              data={this.state.apiData}
              renderItem={this.renderMovieListItem}
              keyExtractor={(item, index) => index.toString()}
              numColumns={2}
              ListEmptyComponent={this.renderEmptyList}
              ListFooterComponent={this.renderFooterForFlatList}
              onEndReached={this.handleMoreListData}
              onEndThreshold={1}
              // onRefresh={this.onRefresh}
              // refreshing={this.state.isRefreshing}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.onRefresh}
                  colors={['#E53935', '#B71C1C']}
                />
              }
            />
          ) : this.state.fetchError ? (
            <View style={styles.loaderViewStyle}>
              <Card>
                <TouchableOpacity onPress={this.onRefresh}>
                  {/* // TODO: Check if on Click Works */}
                  <Text>Not Able to Load Data Use VPN</Text>
                </TouchableOpacity>
              </Card>
            </View>
          ) : (
                <View style={styles.loaderViewStyle}>
                  <ActivityIndicator size='large' color='#B71C1C' />
                </View>
              )
        }
      </ImageBackground>
    );
  }
}

export default MoviesHome;

const styles = {
  headerStyle: {
    flexDirection: 'row',
    backgroundColor: 'rgba(29, 36, 42, 0.86)',
  },
  CardContainerStyle: {
    backgroundColor: 'rgba(29, 36, 42, 0.88)',
    borderWidth: 0
  },
  loaderViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabHeaderStyle: {
    color: '#9EA1A3',
    fontSize: 19
  },
  headerBodyStyle: {
    alignItems: 'flex-start',
    marginLeft: -100
  },
  searchBarContainerStyle: {
    width: ConstantsStrings.SCREEN_WIDTH / 1.65,
    backgroundColor: 'transparent'
  },
  imageBackgroundStyle: {
    height: ConstantsStrings.SCREEN_HEIGHT,
    width: '100%',
    flex: 1
  },
  cardTextStyle: {
    color: '#9EA1A3'
  }
};
