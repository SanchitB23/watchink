import {
  View,
  BackHandler,
  ImageBackground,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid
} from 'react-native';
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Font, Constants } from 'expo';
import firebase from 'firebase';
import {
  Header,
  Left,
  Body,
  Title,
  Footer,
  Form,
  Input,
  Label,
  Item,
  Right,
  Container
} from 'native-base';
import { Button, Icon, SocialIcon } from 'react-native-elements';

import { loginWithEmail } from '../../actions';
import { SCREEN_WIDTH } from '../../utils/constants';

class LoginScreen extends Component {


  constructor() {
    super();
    this.state = {
      fontLoaded: false,
      email: '',
      token: null,
      error: ''
    };
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }
  async componentWillMount() {
    await firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        // this.props.navigation.navigate('main'); TEMP
        this.setState({ token: true });
      } else {
        this.setState({ token: false });
      }
    });
  }
  async componentDidMount() {
    console.log('login');
    await Font.loadAsync({
      Roboto_medium: require('../../../assets/fonts/Roboto-Light.ttf')
    });

    this.setState({ fontLoaded: true });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
  }
  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack(null);
    return true;
  }

  onSubmit(values) {
    this.setState({ error: '' });
    const { email, password } = values;
    this.props.loginWithEmail({ email, password }, this.props.navigation);
  }
  onForgetPassword = async () => {
    try {
      await firebase.auth().sendPasswordResetEmail(this.state.email);
      ToastAndroid.show('Password reset Email is sent', ToastAndroid.SHORT);
    } catch (e) {
      this.setState({ error: e.message });
    }
  }

  onChangeEmail = (email) => this.setState({ email })

  renderField(field) {
    return (
      <View>
        <Item stackedLabel>
          <Label style={styles.textColor}>{field.label}</Label>
          <Input
            style={[styles.textColor]}
            secureTextEntry={field.secureTextEntry}
            {...field.input}
            keyboardType={field.keyboardType}
            autoCapitalize='none'
            onChangeText={field.onChangeText}
          />
        </Item>
        <Text style={{ color: '#D50000', marginLeft: 20, marginTop: 5 }}>
          {field.meta.touched ? field.meta.error : ''}
        </Text>
      </View>
    );
  }
  render() {
    const { handleSubmit } = this.props;
    return (
      <Container>
        <ImageBackground
          source={require('../../../assets/images/bg_login.png')}
          resizeMode='cover'
          style={{ height: '100%', width: '100%', flex: 1 }}
        >
          {
            _.isNull(this.state.token) ? (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' color='#B71C1C' />
              </View>
            ) : (
                <View style={{ flex: 1 }}>
                  <View style={{ height: Constants.statusBarHeight, backgroundColor: '#373E43' }} />
                  <Header span style={{ backgroundColor: 'transparent' }}>
                    <Left>
                      <Icon
                        name='arrow-left'
                        type='feather'
                        color="#fff"
                        style={{ marginLeft: 35 }}
                        onPress={() => this.props.navigation.navigate('welcome')}
                      />
                    </Left>
                    <Body>
                      {
                        this.state.fontLoaded ? (
                          <Title style={{ fontSize: 40 }}>Login</Title>
                        ) : null
                      }
                    </Body>
                    <Right />
                  </Header>
                  <View style={{ flex: 1 }}>

                    <Form style={{ paddingHorizontal: '5%', justifyContent: 'center' }}>
                      <Field
                        label='Email Address'
                        name='email'
                        component={this.renderField}
                        secureTextEntry={false}
                        keyboardType='email-address'
                        onChangeText={this.onChangeEmail}
                      />
                      <Field
                        label='Password'
                        name='password'
                        component={this.renderField}
                        secureTextEntry
                        keyboardType='default'
                        onChangeText={() => { }}
                      />
                    </Form>
                    <View style={styles.error}>
                      <Text style={{ color: '#BF1616', textAlign: 'center' }}>
                        {this.props.error}{this.state.error}
                      </Text>
                    </View>
                    <View style={styles.loginButtonView}>
                      <Button
                        title='Login'
                        buttonStyle={styles.loginButton}
                        onPress={this.props.loading ? null : handleSubmit(this.onSubmit.bind(this))}
                        loading={this.props.loading}
                      />
                      <Text
                        onPress={this.onForgetPassword}
                        style={styles.textColor}
                      >forgot password?</Text>
                    </View>
                    <View style={styles.oAuthView}>
                      <Text
                        style={{ color: '#C9C6C6', marginBottom: '2%' }}
                      >Or, Login with</Text>
                      <View style={{ flexDirection: 'row' }}>
                        <SocialIcon
                          type='facebook'
                          button
                          title='Facebook'
                          style={styles.socialButtonStyle}
                        />

                        <SocialIcon
                          type='google-plus-official'
                          button
                          title='Google'
                          raised
                          style={styles.socialButtonStyle}
                        />
                      </View>
                    </View>
                  </View>

                  <Footer style={styles.footer}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('register')}>
                      <Text style={styles.textColor}>
                        Don't have an account?
                      <Text style={{ color: '#BF1616' }}>
                          Sign Up
                      </Text>
                      </Text>
                    </TouchableOpacity>
                  </Footer>
                </View>
              )
          }
        </ImageBackground>
      </Container>
    );
  }
}

function validate(values) {
  const errors = {};
  if (!values.email) {
    errors.email = 'Enter Email Address';
  }
  if (!values.password) {
    errors.password = 'Enter Password';
  }
  return errors;
}

const mapStateToProps = ({ auth }) => {
  const { error, loading } = auth;
  return { error, loading };
};


export default reduxForm({
  validate,
  form: 'LoginForm'
})(connect(mapStateToProps, { loginWithEmail })(LoginScreen));

const styles = {
  loginButton: {
    backgroundColor: '#BF1616',
    paddingRight: 30,
    paddingLeft: 30,
    borderRadius: 8,
  },
  textColor: {
    color: '#9FA0A2',
  },
  loginButtonView: {
    marginTop: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: SCREEN_WIDTH
  },
  footer: {
    backgroundColor: '#C4C4C423',
    height: 37,
    alignItems: 'center'
  },
  socialButtonStyle: {
    paddingHorizontal: 10,
    paddingVertical: -5
  },
  oAuthView: {
    alignItems: 'center',
    marginTop: '5%',
    justifyContent: 'center',
    width: SCREEN_WIDTH
  },
  error: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  }
};
