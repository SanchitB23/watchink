import React, { Component } from 'react';
import { AppLoading } from 'expo';
import _ from 'lodash';
import { AsyncStorage } from 'react-native';

import Slides from '../Slides';

import * as Constants from '../../utils/constants';

const { SLIDES_DATA } = Constants;

class WelcomeScreen extends Component {
  state = { token: null }
  async componentWillMount() {
    const value = await AsyncStorage.getItem('first_launch');
    if (value) {
      this.setState({ token: true });
      this.props.navigation.navigate('auth');
    } else {
      this.setState({ token: false });
    }
  }

  componentDidMount() {
    console.log('WelcomeScreen');
  }

  onLoginPress() {
    this.props.navigation.navigate('login');
  }
  onRegisterPress() {
    this.props.navigation.navigate('register');
  }

  render() {
    if (_.isNull(this.state.token)) {
      return <AppLoading />; // TODO: Change this to some loader screen
    }
    return (
      <Slides
        data={SLIDES_DATA}
        onLoginPress={this.onLoginPress.bind(this)}
        onRegisterPress={this.onRegisterPress.bind(this)}
      />
    );
  }
}

export default WelcomeScreen;
