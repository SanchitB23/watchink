import { View, ScrollView, Image, ImageBackground } from 'react-native';
import React, { Component } from 'react';
import { Text, Button } from 'react-native-elements';
import { Font } from 'expo';
import * as Constants from '../utils/constants';

const { SCREEN_WIDTH } = Constants;

export default class Slides extends Component {
  state = { fontLoaded: false };
  async componentWillMount() {
    await Font.loadAsync({
      Roboto_medium: require('../../assets/fonts/Roboto-Light.ttf')
    });

    this.setState({ fontLoaded: true });
  }
  renderLastSlide(slide, index) {
    if (index === this.props.data.length - 1) {
      return (
        <View style={{ paddingTop: 90, alignItems: 'center' }}>
          <Button
            title='Login'
            onPress={this.props.onLoginPress}
            buttonStyle={styles.buttonLoginStyle}
            backgroundColor='#3B5998'
            rounded
          />
          <Button
            title='Register'
            onPress={this.props.onRegisterPress}
            rounded
            backgroundColor='#BE1616'
            buttonStyle={styles.buttonRegisterStyle}
          />
        </View>
      );
    }
  }
  renderSlides(slide, index) {
    if (index !== 0 && index !== this.props.data.length - 1) {
      return (
        <Text style={styles.slideText}>{slide.text}</Text>
      );
    }
  }
  // FIXME: Icon getting cropped, figure out a good height: width
  renderAllSlides() {
    return this.props.data.map((slide, index) => (
      <ImageBackground
        key={slide.text}
        style={styles.slide}
        source={slide.bg}
        resizeMode='cover'
      >
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Image
            source={slide.icon}
            style={{ width: slide.width, height: slide.height }}
          />
          <Text style={styles.slideText}>{slide.text}</Text>
        </View>
        {this.renderLastSlide(slide, index)}
      </ImageBackground>
    ));
  }

  render() {
    if (!this.state.fontLoaded) {
      return <Text>loading</Text>;
    }
    return (
      <ScrollView
        horizontal
        pagingEnabled
        style={{ flex: 1 }}
      >
        {this.renderAllSlides()}
      </ScrollView>
    );
  }
}

const styles = {
  slideText: {
    color: 'white',
    paddingTop: 20,
    textAlign: 'center',
    // marginLeft: 20,
    fontFamily: 'Roboto_medium',
    fontSize: 70,
    fontWeight: 'bold',
    textAlignVertical: 'top'
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    width: SCREEN_WIDTH,
  },
  buttonLoginStyle: {
    marginBottom: 40,
    paddingHorizontal: 60,
  },
  buttonRegisterStyle: {
    paddingHorizontal: 50
  },
};
