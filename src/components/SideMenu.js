import {
  View, ImageBackground, Text, TouchableWithoutFeedback, Linking, ToastAndroid
} from 'react-native';
import React, { Component } from 'react';
import { Icon, Divider } from 'react-native-elements';
import { Constants } from 'expo';
import firebase from 'firebase';

//TODO : Static Only, Takes Nowhere
export default class SideMenu extends Component {
  state = {
    emailVerified: false
  }
  componentDidMount() {
    console.log('SideMenu');
    this.emailVerificationCheck();
  }
  onLogOutClick = () => {
    firebase.auth().signOut();
    this.props.navigation.navigate('welcome');
  }
  emailVerificationCheck = async () => {
    await firebase.auth().onAuthStateChanged(async user => {
      if (user) {
        this.setState({ emailVerified: user.emailVerified });
      } else this.setState({ emailVerified: false });
    });
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: Constants.statusBarHeight, backgroundColor: '#373E43' }} />
        <ImageBackground
          source={require('../../assets/images/ic_watchink.png')}
          style={styles.backgroundImageStyle}
          resizeMode='center'
        >
          <Icon
            name='cross'
            type='entypo'
            containerStyle={{
              alignSelf: 'flex-end'
            }}
            onPress={() => this.props.navigation.closeDrawer()}
          />
        </ImageBackground>
        <View style={{ backgroundColor: 'rgba(29, 36, 42, 0.96)', flex: 3 }}>
          <View style={styles.navOptions}>
            <TouchableWithoutFeedback
              onPress={() => {
                ToastAndroid.show('Feature Coming Soon!!', ToastAndroid.SHORT);
                console.log('Favorites');
              }}
            >
              <View style={styles.navOption}>
                <Icon
                  name='star'
                  type='entypo'
                  color='#fff'
                  size={32}
                />
                <Text style={styles.textStyle}>Favorites</Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                console.log('Settings');
                ToastAndroid.show('Feature Coming Soon!!', ToastAndroid.SHORT);
              }}
            >
              <View style={styles.navOption}>
                <Icon
                  name='md-settings'
                  type='ionicon'
                  color='#fff'
                  size={32}
                />
                <Text style={styles.textStyle}>Settings</Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={() => Linking.openURL('https://goo.gl/forms/Faxmjf2s2bLdp1yD2')}>
              <View style={styles.navOption}>
                <Icon
                  name='note'
                  type='simple-line-icon'
                  color='#fff'
                  size={32}
                />
                <Text style={styles.textStyle}>Feedback</Text>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => {
                console.log('About');
                ToastAndroid.show('Feature Coming Soon!!', ToastAndroid.SHORT);
              }}
            >
              <View style={styles.navOption}>
                <Icon
                  name='info-outline'
                  color='#fff'
                  size={32}
                />
                <Text style={styles.textStyle}>About</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <Divider />

          <View style={styles.navOptions} >
            <TouchableWithoutFeedback
              onPress={() => {
                console.log('Account Details');
                ToastAndroid.show('Feature Coming Soon!!', ToastAndroid.SHORT);
              }}
            >
              <View style={styles.navOption}>
                <Icon
                  name='user'
                  type='evilicon'
                  color={this.state.emailVerified ? '#fff' : '#D50000'}
                  size={32}
                />
                <Text style={styles.textStyle}>Account Details</Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={this.onLogOutClick}>
              <View style={styles.navOption}>
                <Icon
                  color='#fff'
                  name='log-out'
                  type='feather'
                  size={32}
                />
                <Text style={styles.textStyle}>Log Out</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  navOptions: {
    paddingVertical: 20,
    paddingLeft: 30,
  },
  navOption: {
    flexDirection: 'row',
    width: '100%',
    marginVertical: 10,
  },
  textStyle: {
    color: '#fff',
    alignSelf: 'center',
    marginLeft: 10,
    fontSize: 24
  },
  backgroundImageStyle: {
    height: '100%',
    width: '100%',
    flex: 1,
    marginVertical: 10
  }
};
