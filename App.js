import React from 'react';
import MainApp from './src';
// import TestComp from './src/Temporary/TestComp';

export default class App extends React.Component {
  render() {
    return (
      // <TestComp />
      <MainApp />
    );
  }
}
